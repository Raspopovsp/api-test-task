import pytest
import requests

base_url = 'https://processing-stage.psmock.com/mnogopay/s/purchase'
secret_key = 'QWERTY000097531KEY'

def test_post_correct():

    headers = {
        'secret': secret_key
    }

    payload = {
        "amount": 100,
        "currency": "RUB",
        "order_id": "testorder222",
        "phone": 76665554433,
        "description": "some description"
        }

    response = requests.post(base_url, headers=headers, json=payload)
    
    assert 200 == response.status_code
    assert '000' == response.json()['resp_code']


@pytest.mark.parametrize('amount, expected_status', [(0.0001, 200), (100, 200), (4000, 200)])
def test_post_with_correct_amounts(amount, expected_status):
    headers = {
        'secret': secret_key
    }

    payload = {
        "amount": amount,
        "currency": "RUB",
        "order_id": "testorder222",
        "phone": 76665554433,
        "description": "some description"
        }

    response = requests.post(base_url, headers=headers, json=payload)
    assert expected_status == response.status_code
    assert '000' == response.json()['resp_code']


@pytest.mark.parametrize('key, expected_status', [('', 403), ('smthn', 403)])
def test_post_with_wrong_auth_key(key, expected_status):

    headers = {
        'secret': key
    }

    payload = {
        "amount": 100,
        "currency": "RUB",
        "order_id": "testorder222",
        "phone": 76665554433,
        "description": "some description"
        }

    response = requests.post(base_url, headers=headers, json=payload)
    assert expected_status == response.status_code


def test_post_with_empty_json():
    headers = {
        'secret': secret_key
    }

    payload = {}

    response = requests.post(base_url, headers=headers, json=payload)
    assert 200 == response.status_code
    assert '222' == response.json()['resp_code']
    assert response.json()['message'] == 'Required parameter is absent : amount, currency, order_id'